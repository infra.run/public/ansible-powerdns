import os
basedir = os.path.abspath(os.path.abspath(os.path.dirname(__file__)))

### BASIC APP CONFIG
SALT = '{{ powerdns__powerdnsadmin_salt }}'
SECRET_KEY = '{{ powerdns__powerdnsadmin_secret_key }}'
HSTS_ENABLED = False
OFFLINE_MODE = True

### DATABASE CONFIG
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = 'postgresql://{{ powerdns__powerdnsadmin_postgres_username }}:{{ powerdns__powerdnsadmin_postgres_password }}@/{{ powerdns__powerdnsadmin_postgres_database }}?host={{ powerdns__postgres_unix_socket_path }}'

# SAML Authnetication
SAML_ENABLED = False
SAML_ASSERTION_ENCRYPTED = True
